       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CONTROL1.
       AUTHOR. KITTIPON THAWEELARB.

       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 NUM1  PIC   9(3) VALUE 9.
       01 NUM2  PIC   9(3) VALUE 15.
       01 NUM3  PIC   9(3) VALUE 2.

       PROCEDURE DIVISION.
       BEGIN.
           IF NUM1 < 10 THEN
              DISPLAY "NUM1 < 1O"
           END-IF 

           DISPLAY "-------------------"
           IF NUM1 LESS THAN 10 
              DISPLAY "NUM1 < 1O"
           END-IF 

           IF NUM1 GREATER THAN OR EQUAL NUM2 THEN
              MOVE NUM1 TO NUM2
           END-IF
           DISPLAY "-------------------"
           DISPLAY "NUM1 : " NUM1 
           DISPLAY "NUM2 : " NUM2 

           IF NUM1 < (NUM2 +(NUM3 / 2)) THEN
              MOVE ZERO TO NUM1 
           END-IF
           DISPLAY "-------------------"
           DISPLAY "NUM1 : " NUM1 

           GOBACK.